﻿using HotChocolate.Types;
using Server.Model;

namespace Server.Types
{
    public class PersonType : ObjectType<Person>
    {
        protected override void Configure(IObjectTypeDescriptor<Person> descriptor)
        {
            base.Configure(descriptor);
            descriptor.Name("Person");
            descriptor.Field(f => f.Id)
                .Type<NonNullType<IdType>>();

            descriptor.Field(f => f.Name)
                .Type<StringType>();

            descriptor.Field(f => f.Age)
                .Type<NonNullType<IntType>>();

            descriptor.Field(f => f.NickName)
                .Type<NonNullType<StringType>>();

            //descriptor.Field(f => f.Friends)
            //    .UsePaging<CharacterType>();

            //descriptor.Field(f => f.AppearsIn)
            //    .Type<ListType<EpisodeType>>();

            //descriptor.Field(f => f.Height)
            //    .Type<FloatType>()
            //    .Argument("unit", a => a.Type<EnumType<Unit>>());
        }
    }
}
