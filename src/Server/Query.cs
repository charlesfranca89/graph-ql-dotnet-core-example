using HotChocolate.Types;
using Server.Types;
using System.Collections.Generic;

namespace Server.Model
{
    public class Query
    {
        public List<Person> people => new List<Person>() {
            new Person() {
                Id = 1,
                Age = 10,
                Name ="Charles",
                NickName = "Test"
            }
        };

        public Person GetPerson(int personId)
        {
            if (personId == 10)
            {
                return new Person()
                {
                    Id = 1,
                    Age = 10,
                    Name = "Charles",
                    NickName = "Test"
                };
            }
            return new Person();
        }
    }

    public class QueryType
        : ObjectType<Query>
    {
        protected override void Configure(IObjectTypeDescriptor<Query> descriptor)
        {

            //.Argument("episode", a => a.DefaultValue(Episode.NewHope)
            descriptor.Field(t => t.people)
                .Type<ListType<PersonType>>();

            descriptor.Field(t => t.GetPerson(default))
                .Type<PersonType>()
                .Argument("personId", a => a.DefaultValue(1));

            //descriptor.Field(t => t.GetCharacter(default, default))
            //    .Type<NonNullType<ListType<NonNullType<CharacterType>>>>();

            //// the search can only be executed if the current
            //// identity has a country
            //descriptor.Field(t => t.Search(default))
            //    .Type<ListType<SearchResultType>>();
        }
    }
}